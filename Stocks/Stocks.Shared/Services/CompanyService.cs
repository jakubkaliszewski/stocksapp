﻿using Stocks.Shared.Model.Models.Companies;
using Stocks.Shared.Model.Models.StockCompanies;
using Stocks.Shared.Model.Models.StockTime;
using Stocks.Shared.Model.Queries;
using Stocks.Shared.Model.Repositories;
using Stocks.Shared.WebAPI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Stocks.Shared.Services
{
    class CompanyService
    {
        private readonly StockCompanyRepository stockCompanyRepository = DependencyInjection.Container.GetInstance<StockCompanyRepository>();
        private readonly CompanyRepository companyRepository = DependencyInjection.Container.GetInstance<CompanyRepository>();
        private readonly CompanyShortInfoRepository companyShortInfoRepository = DependencyInjection.Container.GetInstance<CompanyShortInfoRepository>();
        private readonly FavouriteCompanyRepository favouriteCompanyRepository = DependencyInjection.Container.GetInstance<FavouriteCompanyRepository>();
        private readonly StockTimeRepository stockTimeRepository = DependencyInjection.Container.GetInstance<StockTimeRepository>();

        private readonly DataGetter dataGetter = DependencyInjection.Container.GetInstance<DataGetter>();

        public async Task<IEnumerable<StockCompany>> GetTopNCompanies(int n)
        {
            var gainers = stockCompanyRepository.GetAll();
            if (!gainers.Any() || gainers.First().Date != DateTime.Now.Date)
            {
                gainers = await dataGetter.GetMostGainerStockCompanies();
                stockCompanyRepository.Add(gainers);
            }
            return gainers;
        }

        public IEnumerable<FavouriteCompany> GetFavouriteCompanies()
        {
            return favouriteCompanyRepository.GetAll();
        }

        public FavouriteCompany GetFavouriteCompany(string symbol)
        {
            CompanyQuery query = new CompanyQuery()
            {
                Symbol = symbol
            };
            return favouriteCompanyRepository.GetByQuery(query).FirstOrDefault();
        }

        public async Task<IEnumerable<CompanyShortInfo>> GetAllCompanies()
        {
            return companyShortInfoRepository.GetAll();
        }

        public async Task AddToFavourites(FavouriteCompany company)
        {
            CompanyQuery query = new CompanyQuery()
            {
                Symbol = company.Symbol
            };
            var fullCompany = companyRepository.GetByQuery(query).FirstOrDefault();
            
            if (fullCompany == null)
                fullCompany = await dataGetter.GetCompany(company.Symbol);
            
            if (fullCompany != null)
                company.ChangesInPercentage = fullCompany.ChangesInPercentage;
            
            favouriteCompanyRepository.Add(company);
        }

        public void RemoveFromFavourites(FavouriteCompany company)
        {
            favouriteCompanyRepository.Delete(company);
        }

        /*
        public async Task<IEnumerable<StockTime>> GetStockTimes(string symbol, StockTimeAPIQuery query)
        {
            string args = query?.ToString();
            args = (String.IsNullOrWhiteSpace(args)) ? "" : "?" + args;
            string url = STOCK_HISTORICAL_PRICE_URL + symbol + args;
            string property = "historical";
            var stockTimes = await GetObjectList<StockTime, StockTimeDTO>(url, property);


            var dbQuery = new StockTimeQuery()
            {
                LastRecords = 3,
                StartDate = new DateTime(2020, 3, 19),
                EndDate = new DateTime(2020, 3, 26)
            };
            var stockTimesDB = stockTimeRepository.GetByQuery(dbQuery);

            return stockTimes;
        }
        */
    }
}
