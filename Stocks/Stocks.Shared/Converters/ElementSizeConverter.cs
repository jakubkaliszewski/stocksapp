﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Data;

namespace Stocks.Shared.Converters
{
    public class ElementSizeConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            double result = (double)value;
            if (double.TryParse(parameter as string, out double param))
                result *= param;
            return result;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            double result = (double)value;
            if (double.TryParse(parameter as string, out double param))
                result /= param;
            return result;
        }
    }
}
