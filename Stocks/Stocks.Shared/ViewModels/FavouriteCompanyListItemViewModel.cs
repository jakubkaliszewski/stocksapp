﻿using Stocks.Shared.Model.Models.Companies;
using System;

namespace Stocks.Shared.ViewModels
{
    public class FavouriteCompanyListItemViewModel
    {
        private readonly FavouriteCompany _favouriteCompany;
        public FavouriteCompany FavouriteCompany => _favouriteCompany;
        private string _companySymbol;
        public string CompanySymbol
        {
            get
            {
                return _companySymbol;
            }
        }

        private string _suffixString;
        public string SuffixString
        {
            get
            {
                return _suffixString;
            }
        }

        public FavouriteCompanyListItemViewModel(FavouriteCompany favouriteCompany)
        {
            _favouriteCompany = favouriteCompany;
            _companySymbol = favouriteCompany.Symbol;
            _suffixString = favouriteCompany.Symbol + " " + favouriteCompany.ChangesInPercentage;
        }

        public override string ToString()
        {
            return _suffixString;
        }

        public static CompanyListItemViewModel ToCompanyListItemViewModel(FavouriteCompanyListItemViewModel favouriteCompanyListItemView)
        {
            return new CompanyListItemViewModel(favouriteCompanyListItemView.FavouriteCompany);
        }
    }
}
