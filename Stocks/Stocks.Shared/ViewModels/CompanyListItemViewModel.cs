﻿using Stocks.Shared.Model.Models.Companies;
using Stocks.Shared.Model.Models.StockCompanies;
using System;

namespace Stocks.Shared.ViewModels
{
    public class CompanyListItemViewModel
    {
        private readonly CompanyShortInfo _company;
        public CompanyShortInfo company => _company;
        private string _companySymbol;
        public string CompanySymbol
        {
            get
            {
                return _companySymbol;
            }
        }

        private string _suffixString;
        public string SuffixString
        {
            get
            {
                return _suffixString;
            }
        }

        public CompanyListItemViewModel(CompanyShortInfo company)
        {
            _company = company;
            _companySymbol = company.Symbol;
            _suffixString = company.Symbol + " " + company.CompanyName;
        }

        public CompanyListItemViewModel(StockCompany company)
        {
            _companySymbol = company.ShortName;
            _suffixString = company.ShortName + " " + company.ChangesPercentage;
        }

        public override String ToString()
        {
            return _suffixString;
        }
    }
}
