﻿using Stocks.Shared.Model.Models.News;
using Stocks.Shared.WebAPI;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using Windows.UI.Xaml;

namespace Stocks.Shared.ViewModels
{
    public class NewsPageViewModel : INotifyPropertyChanged
    {
        private readonly NewsGetter _newsGetter;

        public event PropertyChangedEventHandler PropertyChanged;

        private ObservableCollection<NewsEntry> _news;
        public ObservableCollection<NewsEntry> News
        {
            get
            {
                if (_news == null) _news = new ObservableCollection<NewsEntry>();
                return _news;
            }
            set
            {
                _news = value;
                OnPropertyChanged("News");
            }
        }

        private Visibility _loadMoreButtonVisibility;
        public Visibility LoadMoreButtonVisibility
        {
            get => _loadMoreButtonVisibility;
            set
            {
                _loadMoreButtonVisibility = value;
                OnPropertyChanged("LoadMoreButtonVisibility");
            }
        }

        public NewsPageViewModel()
        {
            _newsGetter = new NewsGetter();
            LoadNews();
        }

        async void LoadNews()
        {
            var entries = await _newsGetter.GetNews();
            News = new ObservableCollection<NewsEntry>(entries);
        }

        async public void LoadMoreNews()
        {
            int page = News.Count / 20 + 1;

            var newEntries = await _newsGetter.GetMoreNews(page);

            foreach(NewsEntry entry in newEntries)
                News.Add(entry);

            if (News.Count == _newsGetter.TotalResults)
                LoadMoreButtonVisibility = Visibility.Collapsed;
        }

        async public void OpenNewsWebsite(object clickedEntry)
        {
            var entry = (NewsEntry)clickedEntry;
            await Windows.System.Launcher.LaunchUriAsync(new Uri(entry.Url));
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
