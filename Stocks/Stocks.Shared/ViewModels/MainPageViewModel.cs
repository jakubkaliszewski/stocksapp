﻿using Stocks.Shared.Services;
using Stocks.Shared.Utilities.SuffixTree;
using Stocks.Shared.WebAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Stocks.Shared.ViewModels
{
    public class MainPageViewModel : INotifyPropertyChanged
    {
        private static readonly int TOP_N = 10;
        public event PropertyChangedEventHandler PropertyChanged;

        private const char SuffixTreeTextEnding = '~';
        private const char SuffixTreeCompanySeparator = '#';
        private const char SuffixTreeCompanyIdSeparator = '^';

        private static readonly string[] ReservedCharacters = new string[]
        {
            SuffixTreeTextEnding.ToString(),
            SuffixTreeCompanySeparator.ToString(),
            SuffixTreeCompanyIdSeparator.ToString(),
        };

        private CompanyService _companyService;
        private DataGetter _dataGetter;
        private SuffixTree _suffixTreeAllCompaniesBySymbol;
        private int _maxCompanyNameLength;
        private Dictionary<int, CompanyListItemViewModel> _allCompanies;


        private IEnumerable<CompanyListItemViewModel> _topCompanies;
        public IEnumerable<CompanyListItemViewModel> TopCompanies
        {
            get => _topCompanies ?? (_topCompanies = new List<CompanyListItemViewModel>());
            set
            {
                _topCompanies = value;
                OnPropertyChanged("TopCompanies");
            }
        }

        public void RefreshData()
        {
            topCompaniesTask = new NotifyVoidTaskCompletion(PrepareTopCompanies(TOP_N));
            favouriteCompaniesTask = new NotifyVoidTaskCompletion(PrepareFavouriteCompanies());
        }

        private IEnumerable<FavouriteCompanyListItemViewModel> _favouriteCompanies;
        public IEnumerable<FavouriteCompanyListItemViewModel> FavouriteCompanies
        {
            get => _favouriteCompanies ?? (_favouriteCompanies = new List<FavouriteCompanyListItemViewModel>());
            set
            {
                _favouriteCompanies = value;
                OnPropertyChanged("FavouriteCompanies");
            }
        }

        private List<CompanyListItemViewModel> _suggestedCompanies;
        public List<CompanyListItemViewModel> SuggestedCompanies
        {
            get => _suggestedCompanies ?? (_suggestedCompanies = new List<CompanyListItemViewModel>());
            set
            {
                _suggestedCompanies = value;
                OnPropertyChanged("SuggestedCompanies");
            }
        }

        private bool _matchWholeText;
        public bool MatchWholeText
        {
            get => _matchWholeText;
            set
            {
                _matchWholeText = value;
                OnPropertyChanged("MatchWholeText");
            }
        }

        private bool _matchEachWord;
        public bool MatchEachWord
        {
            get => _matchEachWord;
            set
            {
                _matchEachWord = value;
                OnPropertyChanged("MatchEachWord");
            }
        }

        public NotifyVoidTaskCompletion topCompaniesTask { get; private set; }
        public NotifyVoidTaskCompletion favouriteCompaniesTask { get; private set; }
        public NotifyVoidTaskCompletion suffixTreeTask { get; private set; }

        public MainPageViewModel()
        {
            MatchWholeText = true;
            MatchEachWord = false;
            _suffixTreeAllCompaniesBySymbol = new SuffixTree();
            _allCompanies = new Dictionary<int, CompanyListItemViewModel>();
            _companyService = DependencyInjection.Container.GetInstance<CompanyService>();
            suffixTreeTask = new NotifyVoidTaskCompletion(PrepareSuffixTree());
            topCompaniesTask = new NotifyVoidTaskCompletion(PrepareTopCompanies(TOP_N));
            favouriteCompaniesTask = new NotifyVoidTaskCompletion(PrepareFavouriteCompanies());            
        }

        public async Task PrepareSuffixTree()
        {
            var allCompanies = await _companyService.GetAllCompanies();
            _maxCompanyNameLength = 0;
            _maxCompanyNameLength = 0;
            StringBuilder allCompaniesBuilder = new StringBuilder();

            int id = 0;
            foreach (var company in allCompanies)
            {
                string name = company.Symbol + ' ' + company.CompanyName + SuffixTreeCompanyIdSeparator + id;
                name = name.ToUpperInvariant();
                allCompaniesBuilder.Append(name + SuffixTreeCompanySeparator);

                if (name.Length > _maxCompanyNameLength)
                    _maxCompanyNameLength = name.Length;

                CompanyListItemViewModel companyListItemViewModel = new CompanyListItemViewModel(company);
                _allCompanies.Add(id, companyListItemViewModel);
                id++;
            }
            allCompaniesBuilder.Length--;
            allCompaniesBuilder.Append(SuffixTreeTextEnding);

            string allCompaniesBySymbols = allCompaniesBuilder.ToString();
            _suffixTreeAllCompaniesBySymbol.Build(allCompaniesBySymbols);
        }

        public async Task PrepareTopCompanies(int topN)
        {
            var topNCompanies = await _companyService.GetTopNCompanies(topN);
            TopCompanies = new List<CompanyListItemViewModel>(topNCompanies.Select(x => new CompanyListItemViewModel(x)));            
        }

        public async Task PrepareFavouriteCompanies()
        {
            var favouriteCompanies = _companyService.GetFavouriteCompanies();
            FavouriteCompanies = new List<FavouriteCompanyListItemViewModel>(favouriteCompanies.Select(x => new FavouriteCompanyListItemViewModel(x)));            
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public static string GetCompanySymbol(string companyText)
        {
            if (!String.IsNullOrWhiteSpace(companyText))
            {
                companyText = companyText.Trim();
                companyText = companyText.Split(' ').FirstOrDefault();
                if (!String.IsNullOrWhiteSpace(companyText))
                    companyText = companyText.Trim();
            }

            if (String.IsNullOrWhiteSpace(companyText))
                companyText = "";

            return companyText;
        }

        public void UpdateCompaniesSearcher(string userText)
        {
            if (String.IsNullOrWhiteSpace(userText))
            {
                SuggestedCompanies = new List<CompanyListItemViewModel>();
                foreach (var company in FavouriteCompanies)
                {
                    SuggestedCompanies.Add(FavouriteCompanyListItemViewModel.ToCompanyListItemViewModel(company));
                }
            }
                
            else
            {
                userText = userText.Trim();
                foreach (string character in ReservedCharacters)
                {
                    userText = userText.Replace(character, "");
                }

                HashSet<int> companiesIds = new HashSet<int>();
                SuggestedCompanies = new List<CompanyListItemViewModel>();

                List<int> substringPositions = new List<int>();

                if (MatchEachWord)
                {
                    var words = userText.Split(' ', StringSplitOptions.RemoveEmptyEntries);
                    foreach (var word in words)
                    {
                        substringPositions.AddRange(_suffixTreeAllCompaniesBySymbol.FindSubstring(word.ToUpperInvariant()));
                    }
                }
                else
                    substringPositions.AddRange(_suffixTreeAllCompaniesBySymbol.FindSubstring(userText.ToUpperInvariant()));

                foreach (int position in substringPositions)
                {
                    int substringLength = _maxCompanyNameLength;
                    if (position + _maxCompanyNameLength > _suffixTreeAllCompaniesBySymbol.Text.Length)
                        substringLength = _suffixTreeAllCompaniesBySymbol.Text.Length - position;

                    string substring = _suffixTreeAllCompaniesBySymbol.Text.Substring(position, substringLength);
                    TryAddIdFromSubstring(companiesIds, substring);
                }

                foreach (int id in companiesIds)
                {
                    var company = _allCompanies[id];
                    SuggestedCompanies.Add(company);
                }
            }
        }

        private static void TryAddIdFromSubstring(HashSet<int> companiesIds, string substring)
        {
            string company = substring.Split(SuffixTreeCompanySeparator).FirstOrDefault();
            if (!String.IsNullOrEmpty(company))
            {
                string idFromCompany = company.Split(SuffixTreeCompanyIdSeparator).LastOrDefault();
                if (int.TryParse(idFromCompany, out int id))
                    companiesIds.Add(id);
            }
        }

        public bool CompanyExists(string companyName)
        {
            bool exists = false;
            companyName = companyName.ToUpperInvariant();
            var substringPositions = _suffixTreeAllCompaniesBySymbol.FindSubstring(companyName);
            foreach (int position in substringPositions)
            {
                if (position == 0 || _suffixTreeAllCompaniesBySymbol.Text[position - 1] == SuffixTreeCompanySeparator)
                {
                    if (_suffixTreeAllCompaniesBySymbol.Text.Substring(position, companyName.Length) == companyName)
                        exists = true;
                }
            }

            return exists;
        }
    }
}
