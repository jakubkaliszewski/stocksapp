﻿using Stocks.Shared.WebAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Threading.Tasks;

namespace Stocks.Shared.ViewModels
{
    public class CompaniesViewModel : INotifyPropertyChanged
    {
        private DataGetter _dataGetter;

        public event PropertyChangedEventHandler PropertyChanged;

        private string companyName;
        public string CompanyName
        {
            get
            {
                return companyName;
            }
            set
            {
                companyName = value;
                OnPropertyChanged("CompanyName");
            }
        }

        private string changesInPercentage;
        public string ChangesInPercentage
        {
            get => changesInPercentage;
            set
            {
                changesInPercentage = value;
                OnPropertyChanged("ChangesInPercentage");
            }
        }

        private string ceo;
        public string CEO
        {
            get => ceo;
            set
            {
                ceo = value;
                OnPropertyChanged("CEO");
            }
        }

        private string description;
        public string Description
        {
            get => description;
            set
            {
                description = value;
                OnPropertyChanged("Description");
            }
        }

        private string marketCapitalization;
        public string MarketCapitalization
        {
            get => marketCapitalization;
            set
            {
                marketCapitalization = value;
                OnPropertyChanged("MarketCapitalization");
            }
        }


        private string price;
        public string Price
        {
            get => price;
            set
            {
                price = value;
                OnPropertyChanged("Price");
            }
        }

        private string sector;
        public string Sector
        {
            get => sector;
            set
            {
                sector = value;
                OnPropertyChanged("Sector");
            }
        }

        private string industry;
        public string Industry
        {
            get => industry;
            set
            {
                industry = value;
                OnPropertyChanged("Industry");
            }
        }

        private string lastDividend;
        public string LastDividend
        {
            get => lastDividend;
            set
            {
                lastDividend = value;
                OnPropertyChanged("LastDividend");
            }
        }


        public string ChosenCompany { get; set; }
        public IEnumerable<string> Companies { get; private set; }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public CompaniesViewModel()
        {
            _dataGetter = new DataGetter();
            Companies = new string[]
            {
                "AAPL", "NVDA", "FB", "WBA", "GOOG", "PIH", "MSFT", "ZNGA", "NVR"
            };
        }

        private Command _getCompaniesCommand;
        public Command GetCompaniesCommand
        {
            get
            {
                if (_getCompaniesCommand == null)
                    _getCompaniesCommand = new Command(async x => { await GetCompanies(); });

                return _getCompaniesCommand;
            }
        }

        private async Task GetCompanies()
        {            
            Console.WriteLine($"Wybrana firma to: {ChosenCompany}");
            if (!String.IsNullOrWhiteSpace(ChosenCompany))
            {
                var company = await _dataGetter.GetCompany(ChosenCompany);
                CompanyName = "Nazwa: " + company.CompanyName;
                ChangesInPercentage = "Zmiana procentowa wartości: " + company.ChangesInPercentage;
                CEO = "CEO: " + company.CEO;
                Description = "Opis: " + company.Description;
                MarketCapitalization = "Wartość kapitalizacji na rynku: " + company.MarketCapitalization;
                Price = "Aktualna cena: " + company.Price;
                Sector = "Sektor: " + company.Sector;
                Industry = "Branża: " + company.Industry;
                LastDividend = "Wartość ostatniej dywidendy: " + company.LastDividend;
            }
        }
    }
}
