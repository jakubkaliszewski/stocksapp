﻿using Microcharts;
using Stocks.Shared.Model.Models.Companies;
using Stocks.Shared.Services;
using Stocks.Shared.WebAPI;
using Stocks.Shared.WebAPI.StockTimeAPI;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using Uno.Extensions.Specialized;
using Windows.Networking.Sockets;

namespace Stocks.Shared.ViewModels
{
    public class CompanyPageViewModel : INotifyPropertyChanged
    {
        private DataGetter _dataGetter;
        private readonly CompanyService _companyService;

        public event PropertyChangedEventHandler PropertyChanged;

        private string _companySymbol;
        public string CompanySymbol
        {
            get => _companySymbol;
            set
            {
                _companySymbol = value;
                OnPropertyChanged("CompanySymbol");
                GetHistoricalData();
            }
        }

        private string _companyName;
        public string CompanyName
        {
            get => _companyName;
            set
            {
                _companyName = value;
                OnPropertyChanged("CompanyName");
            }
        }

        private string _companyDescription;
        public string CompanyDescription
        {
            get => _companyDescription;
            set
            {
                _companyDescription = value;
                OnPropertyChanged("CompanyDescription");
            }
        }

        private IEnumerable<string> _stockTimes;
        public IEnumerable<string> StockTimes
        {
            get => _stockTimes;
            set
            {
                _stockTimes = value;
                OnPropertyChanged("StockTimes");
            }
        }

        private Chart _chart;
        public Chart Chart
        {
            get
            {
                if (_chart == null) _chart = new LineChart() { Entries = new List<ChartEntry>(), BackgroundColor = SkiaSharp.SKColors.Transparent, IsAnimated = false };
                return _chart;
            }
            set
            {
                _chart = value;
                OnPropertyChanged("Chart");
            }
        }


        private DateTime _startDate;
        public DateTime StartDate
        {
            get => _startDate;
            set
            {
                _startDate = value;
                LastRecords = 0;
                OnPropertyChanged("StartDate");
            }
        }

        private DateTime _endDate;
        public DateTime EndDate
        {
            get => _endDate;
            set
            {
                _endDate = value;
                LastRecords = 0;
                OnPropertyChanged("EndDate");
            }
        }

        private int _lastRecords;
        public int LastRecords
        {
            get => _lastRecords;
            set
            {
                _lastRecords = value;
                OnPropertyChanged("LastRecords");
            }
        }

        private bool _usePredictions = true;
        public bool UsePredictions
        {
            get => _usePredictions;
            set
            {
                _usePredictions = value;
                GetHistoricalData();
                OnPropertyChanged("UsePredictions");
            }
        }

        public CompanyPageViewModel()
        {
            _dataGetter = new DataGetter();
            _companyService = DependencyInjection.Container.GetInstance<CompanyService>();
            StartDate = DateTime.Now.AddDays(-7);
            EndDate = DateTime.Now;
            LastRecords = 0;
        }

        public async Task GetHistoricalData(StocksTimeSpan timeSpan = StocksTimeSpan.SpecifiedByDates)
        {
            Console.WriteLine($"Wybrana firma to: {CompanySymbol}");
            if (!String.IsNullOrWhiteSpace(CompanySymbol))
            {
                if (timeSpan != StocksTimeSpan.SpecifiedByLastRecords)
                    LastRecords = 0;

                (DateTime startDate, DateTime endDate) = GetDates(timeSpan);

                StockTimeAPIQuery query = new StockTimeAPIQuery()
                {
                    StartDate = LastRecords > 0 ? default : startDate,
                    EndDate = LastRecords > 0 ? default : endDate,
                    LastRecords = LastRecords
                };
                try
                {
                    await GetCompanyDetails();
                    await GetCompanyPrices(query);
                }
                catch (Exception e)
                {
                    StockTimes = new List<string>();
                    CompanyDescription = "";
                    CompanyName = CompanySymbol;
                }
            }
        }

        private (DateTime, DateTime) GetDates(StocksTimeSpan timeSpan)
        {
            DateTime startDate = StartDate;
            DateTime endDate = EndDate;

            if (timeSpan != StocksTimeSpan.SpecifiedByDates)
            {
                endDate = DateTime.Now.Date;

                if (timeSpan == StocksTimeSpan.Week)
                    startDate = endDate.AddDays(-7);

                if (timeSpan == StocksTimeSpan.Month)
                    startDate = endDate.AddDays(-30);

                if (timeSpan == StocksTimeSpan.Year)
                    startDate = endDate.AddDays(-365);
            }

            return (startDate, endDate);
        }

        private async Task GetCompanyPrices(StockTimeAPIQuery query)
        {
            var stockTimes = (await _dataGetter.GetStockTimes(CompanySymbol, query))
                                .OrderBy(x => x.Date)
                                .ToList();

            StockTimes = stockTimes.Select(x => x.ToString());

            var entries = new List<ChartEntry>();
            decimal minValue = decimal.MaxValue;
            decimal maxValue = decimal.MinValue;
            DateTime minDate = DateTime.MaxValue;
            DateTime maxDate = DateTime.MinValue;

            bool predictionUsed = false;
            decimal[] predictions = null;

            if (UsePredictions)
            {
                try
                {
                    predictions = Prediction.Predict(stockTimes);
                    predictionUsed = true;
                }
                catch (Exception e)
                {
                    predictionUsed = false;
                }
            }

            int valueCount = predictionUsed ? predictions.Length + stockTimes.Count : stockTimes.Count;

            int showLabelInterval = (int)Math.Ceiling(valueCount / 8.0);
            int intervalCount = 0;

            for (int i = 0; i < stockTimes.Count; i++)
            {
                var stockTime = stockTimes[i];

                if (stockTime.Close < minValue)
                    minValue = stockTime.Close;

                if (stockTime.Close > maxValue)
                    maxValue = stockTime.Close;

                if (stockTime.Date < minDate)
                    minDate = stockTime.Date;

                if (stockTime.Date > maxDate)
                    maxDate = stockTime.Date;

                var entry = new ChartEntry((float)stockTime.Close)
                {
                    Color = SkiaSharp.SKColors.White,
                    TextColor = SkiaSharp.SKColors.White,
                };

                if (i == intervalCount * showLabelInterval || intervalCount * showLabelInterval == 0 || (i == stockTimes.Count - 1 && !predictionUsed))
                {
                    intervalCount++;
                    //entry.Label = stockTime.Date.ToString("dd-MM");
                    //label nie moze byc nullem (exception) i nie moze byc pusty (problemy ze skalowaniem wykresu)
                    entry.Label = " ";
                    entry.ValueLabel = stockTime.Close.ToString();
                }

                entries.Add(entry);
            }

            //TODO: Dopasowac parametry predykcji
            if (predictionUsed)
            {
                var lastDate = maxDate;
                int offset = stockTimes.Count;

                for (int i = 0; i < predictions.Length; i++)
                {
                    var stockDate = lastDate.AddDays(i + 1);

                    if (predictions[i] < minValue)
                        minValue = predictions[i];

                    if (predictions[i] > maxValue)
                        maxValue = predictions[i];

                    if (stockDate < minDate)
                        minDate = stockDate;

                    if (stockDate > maxDate)
                        maxDate = stockDate;

                    var entry = new ChartEntry((float)predictions[i])
                    {
                        Color = SkiaSharp.SKColors.LightGreen,
                        TextColor = SkiaSharp.SKColors.LightGreen,
                    };

                    if (i + offset == intervalCount * showLabelInterval || i == predictions.Length - 1)
                    {
                        intervalCount++;
                        //entry.Label = stockTime.Date.ToString("dd-MM");
                        //label nie moze byc nullem (exception) i nie moze byc pusty (problemy ze skalowaniem wykresu)
                        entry.Label = " ";
                        entry.ValueLabel = predictions[i].ToString();
                    }

                    entries.Add(entry);
                }
            }

            float margin = (float)(maxValue - minValue) * 0.1f;

            Chart = new LineChart()
            {
                Entries = entries,
                BackgroundColor = SkiaSharp.SKColors.Transparent,
                IsAnimated = false,
                LabelTextSize = 12,
                LineMode = (entries.Count > 50 ? LineMode.Straight : LineMode.Spline),
                ValueLabelOrientation = Orientation.Horizontal,
                MinValue = (float)(minValue) - margin,
                MaxValue = (float)(maxValue) + margin,
                PointSize = entries.Count >= 50 ? 5 : 10,
                LineSize = entries.Count >= 50 ? 1 : 3,
            };


            int lastRecords = LastRecords;
            StartDate = minDate;
            EndDate = maxDate;
            LastRecords = lastRecords;
        }

        private async Task GetCompanyDetails()
        {
            var company = await _dataGetter.GetCompany(CompanySymbol);
            if (company != null)
            {
                var description = new List<string>()
                {
                    $"Company symbol: {company.Symbol}",
                    $"Company name: {company.CompanyName}",
                    $"Industry: {company.Industry}",
                    $"Sector: {company.Sector}",
                    $"CEO: {company.CEO}",
                    $"Website: {company.Website}",
                    "",
                    $"Description: {company.Description}"
                };
                CompanyDescription = String.Join("\n", description);
                CompanyName = $"{company.CompanyName} ({company.Symbol})";
            }
        }

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public async Task AddToFavourites()
        {
            FavouriteCompany favouriteCompany = _companyService.GetFavouriteCompany(CompanySymbol);
            if (favouriteCompany == null)
            {
                var company = await _dataGetter.GetCompany(CompanySymbol);
                if (company != null)
                {
                    favouriteCompany = new FavouriteCompany()
                    {
                        Symbol = company.Symbol,
                        CompanyName = company.CompanyName,
                        Exchange = company.Exchange,
                        Price = company.Price
                    };
                    _companyService.AddToFavourites(favouriteCompany);
                }
            }
        }

        public void RemoveFromFavourites()
        {
            FavouriteCompany favouriteCompany = _companyService.GetFavouriteCompany(CompanySymbol);
            if (favouriteCompany != null)
                _companyService.RemoveFromFavourites(favouriteCompany);
        }

        public bool IsFavourite()
        {
            var company = _companyService.GetFavouriteCompany(CompanySymbol);
            return company != null;
        }
    }

    public enum StocksTimeSpan
    {
        SpecifiedByLastRecords,
        SpecifiedByDates,
        Week,
        Month,
        Year
    }
}
