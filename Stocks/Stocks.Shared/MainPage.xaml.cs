﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Threading.Tasks;
using System.Net.Http;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Windows.Media.Casting;
using Stocks.Shared.Pages;
using Stocks.Shared;
using System.Runtime.InteropServices.ComTypes;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=402352&clcid=0x409

namespace Stocks
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public partial class MainPage : Page
    {
        public static MainPage ThisPage { get; private set; }
        public MainPage()
        {
            this.InitializeComponent();
            SplitViewFrame.Navigate(typeof(TopNewsPage));
            ThisPage = this;
        }
        
        public static void RefreshData()
        {
            var model = ThisPage.DataContext as Shared.ViewModels.MainPageViewModel;
            model.RefreshData();
        }

        private void MainPage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MainGrid.Height = e.NewSize.Height;
            rpStockLists.Height = e.NewSize.Height;
            double magicFactor = 0.3;
#if __ANDROID__
            magicFactor = 0.1;
#endif
            double height = (rpStockLists.Height - TopStocks.ActualHeight - Favourites.ActualHeight) * magicFactor;
            svFavourites.Height = height;
            rpFavouriteCompanies.Height = svFavourites.Height;
            lvFavouriteCompanies.Height = rpFavouriteCompanies.Height;

            svTop.Height = height;
            rpTopCompanies.Height = svTop.Height;
            lvTopCompanies.Height = rpTopCompanies.Height;
        }


        private void BackButton_Click(object sender, RoutedEventArgs e)
        {
            if (SplitViewFrame.CanGoBack)
                SplitViewFrame.GoBack();
        }


        private void GoToNewsButton_Click(object sender, RoutedEventArgs e)
        {
            SplitViewFrame.Navigate(typeof(TopNewsPage));
            lvFavouriteCompanies.SelectedItem = null;
            lvTopCompanies.SelectedItem = null;
        }

        private void AutoSuggestBox_TextChanged(AutoSuggestBox sender, AutoSuggestBoxTextChangedEventArgs args)
        {
            if (args.Reason == AutoSuggestionBoxTextChangeReason.UserInput)
            {
                var viewModel = DataContext as Shared.ViewModels.MainPageViewModel;
                if (viewModel != null)
                    viewModel.UpdateCompaniesSearcher(sender.Text);
            }
        }

        private void AutoSuggestBox_SuggestionChosen(AutoSuggestBox sender, AutoSuggestBoxSuggestionChosenEventArgs args)
        {
            var selectedItem = args.SelectedItem.ToString();
            sender.Text = selectedItem;
        }

        private void AutoSuggestBox_QuerySubmitted(AutoSuggestBox sender, AutoSuggestBoxQuerySubmittedEventArgs args)
        {
            if (args.ChosenSuggestion != null)
            {
                sender.Text = args.ChosenSuggestion.ToString();
                string symbol = Shared.ViewModels.MainPageViewModel.GetCompanySymbol(sender.Text);
                SplitViewFrame.Navigate(typeof(CompanyPage), symbol);
            }
            else
            {
                var viewModel = DataContext as Shared.ViewModels.MainPageViewModel;
                if (viewModel != null)
                {
                    string symbol = Shared.ViewModels.MainPageViewModel.GetCompanySymbol(args.QueryText);
                    if (viewModel.CompanyExists(symbol))
                        SplitViewFrame.Navigate(typeof(CompanyPage), symbol);
                }
            }
        }

        private void ListView_ItemClick(object sender, ItemClickEventArgs e)
        {
            string item = e.ClickedItem.ToString();
            string symbol = Shared.ViewModels.MainPageViewModel.GetCompanySymbol(item);
            if (!String.IsNullOrWhiteSpace(symbol))
                SplitViewFrame.Navigate(typeof(CompanyPage), symbol);                
        }       
    }
}