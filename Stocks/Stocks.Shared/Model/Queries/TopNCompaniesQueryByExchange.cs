﻿using Stocks.Shared.Model.Models.Companies;

namespace Stocks.Shared.Model.Queries
{
    class TopNCompaniesQueryByExchange : IQuery<CompanyShortInfo>
    {
        public int LimitRecords { get; set; }

        public string BuildQuery(string tableName)
        {
            string query = $"SELECT * FROM {tableName} ORDER BY Price DESC LIMIT {LimitRecords}".TrimEnd();
            return query;
        }
    }
}
