﻿using Stocks.Shared.Model.Models.StockTime;
using System;
using System.Collections.Generic;

namespace Stocks.Shared.Model.Queries
{
    class StockTimeQuery : IQuery<StockTime>
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LastRecords { get; set; }

        public string BuildQuery(string tableName)
        {
            List<string> conditions = new List<string>();
            string limit = "";
            string orderBy = "";

            if (LastRecords > 0)
            {
                limit = $"LIMIT {LastRecords}";
                orderBy = "ORDER BY Date DESC";
            }

            if (StartDate != DateTime.MinValue)
                conditions.Add($"Date >= {StartDate.Ticks}");

            if (EndDate != DateTime.MinValue)
                conditions.Add($"Date <= {EndDate.Ticks}");

            string condition = String.Join(" AND ", conditions);
            if (!String.IsNullOrWhiteSpace(condition))
                condition = "WHERE " + condition;

            string query = $"SELECT * FROM {tableName} {condition} {orderBy} {limit}".TrimEnd();
            return query;
        }
    }
}
