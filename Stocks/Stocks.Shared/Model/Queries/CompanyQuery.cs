﻿using Stocks.Shared.Model.Models.Companies;
using System;
using System.Collections.Generic;
using System.Text;

namespace Stocks.Shared.Model.Queries
{
    class CompanyQuery : IQuery<Company>, IQuery<FavouriteCompany>
    {
        public string Symbol { get; set; }
        public string BuildQuery(string tableName)
        {
            List<string> conditions = new List<string>();
            if (!String.IsNullOrWhiteSpace(Symbol))
                conditions.Add($"Symbol = '{Symbol}'");

            string condition = String.Join(" AND ", conditions);
            if (!String.IsNullOrWhiteSpace(condition))
                condition = "WHERE " + condition;

            string query = $"SELECT * FROM {tableName} {condition}".TrimEnd();
            return query;
        }
    }
}
