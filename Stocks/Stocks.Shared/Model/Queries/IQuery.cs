﻿using Stocks.Shared.Model.Models.Interfaces;

namespace Stocks.Shared.Model.Queries
{
    interface IQuery<T> 
        where T : StockEntityObject
    {
        string BuildQuery(string tableName);
    }
}
