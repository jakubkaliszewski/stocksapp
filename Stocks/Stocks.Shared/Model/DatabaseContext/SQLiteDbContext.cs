﻿#if __ANDROID__
using Android.OS;
#endif
using SQLite;
using Stocks.Shared.Model.Models.Companies;
using Stocks.Shared.Model.Models.StockCompanies;
using Stocks.Shared.Model.Models.StockIndex;
using Stocks.Shared.Model.Models.StockTime;
using System.IO;
#if NETFX_CORE
using Windows.Storage;
#endif

namespace Stocks.Shared.Model.DatabaseContext
{
    public class SQLiteDbConnection
    {
        private static string _connectionString = GetConnectionStringPath("stocks.db");

        public SQLiteConnection Db { get; }

        public SQLiteDbConnection()
        {
            Db = new SQLiteConnection(_connectionString);
            Db.CreateTables(CreateFlags.None, new System.Type[] {
                typeof(Company),
                typeof(StockCompany),
                typeof(StockIndex),
                typeof(StockTime),
                typeof(FavouriteCompany),
                typeof(CompanyShortInfo)
            });
        }

        private static string GetConnectionStringPath(string databaseFileName)
        {
            string path = "";
#if __ANDROID__
            path = Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.ApplicationData), databaseFileName);
#endif
#if NETFX_CORE
            path = Path.Combine(ApplicationData.Current.LocalFolder.Path, databaseFileName);
#endif
#if __WASM__
            path = databaseFileName;
#endif
            return path;
        }
    }
}