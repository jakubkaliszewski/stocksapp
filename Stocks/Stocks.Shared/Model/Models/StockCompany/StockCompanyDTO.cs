﻿using Stocks.Shared.Model.Models.Interfaces;
using System;

namespace Stocks.Shared.Model.Models.StockCompanies
{
    public class StockCompanyDTO : StockObjectDTO<StockCompany>
    {
        public string Ticker { get; set; }
        public decimal Changes { get; set; }
        public decimal Price { get; set; }
        public string ChangesPercentage { get; set; }
        public string CompanyName { get; set; }

        public override StockCompany ToStockObject()
        {
            StockCompany stockCompany = new StockCompany()
            {
                ShortName = Ticker,
                Changes = Changes,
                Price = Price,
                ChangesPercentage = ChangesPercentage,
                CompanyName = CompanyName,
                Date = DateTime.Now.Date
            };
            return stockCompany;
        }
    }
}
