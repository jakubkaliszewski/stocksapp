﻿using Stocks.Shared.Model.Models.Interfaces;
using SQLite;
using System;

namespace Stocks.Shared.Model.Models.StockCompanies
{
    /// <summary>
    /// Spółka akcyjna
    /// </summary>
    [Table("StockCompanies")]
    public class StockCompany : StockEntityObject
    {
        public string ShortName { get; set; }
        public decimal Changes { get; set; }
        public decimal Price { get; set; }
        public string ChangesPercentage { get; set; }
        public string CompanyName { get; set; }
        public DateTime Date { get; set; }
    }
}
