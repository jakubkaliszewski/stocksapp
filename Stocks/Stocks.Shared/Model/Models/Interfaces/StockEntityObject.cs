﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace Stocks.Shared.Model.Models.Interfaces
{
    /// <summary>
    /// Obiekt encyjny
    /// </summary>
    public abstract class StockEntityObject : StockObject
    {
        [PrimaryKey, AutoIncrement]
        public int Id { get; set; }
    }
}
