﻿namespace Stocks.Shared.Model.Models.Interfaces
{
    public abstract class StockObjectDTO<T> where T : StockObject
    {
        public static explicit operator T(StockObjectDTO<T> stockObjectDTO)
        {
            return stockObjectDTO.ToStockObject();
        }
        public abstract T ToStockObject();
    }
}
