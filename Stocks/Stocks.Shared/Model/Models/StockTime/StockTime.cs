﻿using Stocks.Shared.Model.Models.Interfaces;
using SQLite;
using System;

namespace Stocks.Shared.Model.Models.StockTime
{
    [Table("StockTimes")]
    public class StockTime : StockEntityObject
    {
        public DateTime Date { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Volume { get; set; }
        public decimal UnadjustedVolume { get; set; }
        public decimal Change { get; set; }
        public decimal ChangePercent { get; set; }
        public decimal Vwap { get; set; }
        public string Label { get; set; }
        public decimal ChangeOverTime { get; set; }

        public override string ToString()
        {
            string result = $"Data: {Date.ToString("yyyy-MM-dd")}, wartość zamknięcia: {Close.ToString("0.00")}";
            return result;
        }
    }
}
