﻿using Stocks.Shared.Model.Models.Interfaces;
using System;

namespace Stocks.Shared.Model.Models.StockTime
{
    public class StockTimeDTO : StockObjectDTO<StockTime>
    {
        public DateTime Date { get; set; }
        public decimal Open { get; set; }
        public decimal High { get; set; }
        public decimal Low { get; set; }
        public decimal Close { get; set; }
        public decimal Volume { get; set; }
        public decimal UnadjustedVolume { get; set; }
        public decimal Change { get; set; }
        public decimal ChangePercent { get; set; }
        public decimal Vwap { get; set; }
        public string Label { get; set; }
        public decimal ChangeOverTime { get; set; }

        public override StockTime ToStockObject()
        {
            StockTime stockTime = new StockTime()
            {
                Date = Date,
                Open = Open,
                High = High,
                Low = Low,
                Close = Close,
                Volume = Volume,
                UnadjustedVolume = UnadjustedVolume,
                Change = Change,
                ChangePercent = ChangePercent,
                Vwap = Vwap,
                Label = Label,
                ChangeOverTime = ChangeOverTime,
            };
            return stockTime;
        }
    }
}
