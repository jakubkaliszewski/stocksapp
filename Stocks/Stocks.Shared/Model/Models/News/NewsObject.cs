﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stocks.Shared.Model.Models.News
{
    public class NewsObject
    {
        public string Status { get; set; }
        public int TotalResults { get; set; }
        public IList<NewsEntry> Articles { get; set; }
    }
}
