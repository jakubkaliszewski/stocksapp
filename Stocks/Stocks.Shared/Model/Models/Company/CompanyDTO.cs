﻿using Stocks.Shared.Model.Models.Interfaces;

namespace Stocks.Shared.Model.Models.Companies
{
    public class CompanyDTO : StockObjectDTO<Company>
    {
        public string Symbol { get; set; }
        public Profile Profile { get; set; }

        public override Company ToStockObject()
        {
            Company company = new Company()
            {
                Symbol = Symbol,
                Price = Profile.Price,
                Beta = Profile.Beta,
                VolumeAverage = Profile.VolAvg,
                MarketCapitalization = Profile.MktCap,
                LastDividend = Profile.LastDiv,
                WeekRange = Profile.Range,
                Changes = Profile.Changes,
                ChangesInPercentage = Profile.ChangesPercentage,
                CompanyName = Profile.CompanyName,
                Exchange = Profile.Exchange,
                Industry = Profile.Industry,
                Website = Profile.Website,
                Description = Profile.Description,
                CEO = Profile.CEO,
                Sector = Profile.Sector,
                ImageUrl = Profile.Image,
            };

            return company;
        }
    }

    public class Profile
    {
        public decimal Price { get; set; }
        public decimal Beta { get; set; }
        public decimal VolAvg { get; set; }
        public decimal MktCap { get; set; }
        public decimal LastDiv { get; set; }
        public string Range { get; set; }
        public decimal Changes { get; set; }
        public string ChangesPercentage { get; set; }
        public string CompanyName { get; set; }
        public string Exchange { get; set; }
        public string Industry { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
        public string CEO { get; set; }
        public string Sector { get; set; }
        public string Image { get; set; }
    }
}
