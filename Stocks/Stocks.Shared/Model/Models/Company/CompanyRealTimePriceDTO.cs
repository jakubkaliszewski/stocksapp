﻿using Stocks.Shared.Model.Models.Interfaces;

namespace Stocks.Shared.Model.Models.Companies
{
    public class CompanyRealTimePriceDTO : StockObjectDTO<CompanyRealTimePrice>
    {
        public string Symbol { get; set; }
        public decimal Price { get; set; }
        public override CompanyRealTimePrice ToStockObject()
        {
            CompanyRealTimePrice companyRealTimePrice = new CompanyRealTimePrice()
            {
                Symbol = Symbol,
                Price = Price
            };

            return companyRealTimePrice;
        }
    }
}
