﻿using SQLite;
using Stocks.Shared.Model.Models.Interfaces;

namespace Stocks.Shared.Model.Models.Companies
{
    [Table("CompaniesShortInfo")]
    public class CompanyShortInfo : StockEntityObject
    {
        public string Symbol { get; set; }
        public string CompanyName { get; set; }
        public decimal Price { get; set; }
        [Indexed]
        public string Exchange { get; set; }
    }
}
