﻿using Stocks.Shared.Model.Models.Interfaces;
using SQLite;

namespace Stocks.Shared.Model.Models.Companies
{
    /// <summary>
    /// Ulubiona spółka akcyjna użytkownika - gwiazdka
    /// </summary>
    [Table("FavouriteCompanies")]
    public class FavouriteCompany : CompanyShortInfo
    {
        public string ChangesInPercentage { get; set; }
    }
}
