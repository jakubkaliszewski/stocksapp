﻿using Stocks.Shared.Model.Models.Interfaces;

namespace Stocks.Shared.Model.Models.Companies
{
    public class CompanyShortInfoDTO : StockObjectDTO<CompanyShortInfo>
    {
        public string Symbol { get; set; }
        public string Name { get; set; }
        public decimal Price { get; set; }
        public string Exchange { get; set; }

        public override CompanyShortInfo ToStockObject()
        {
            CompanyShortInfo companyShortInfo = new CompanyShortInfo()
            {
                Symbol = Symbol,
                CompanyName = Name,
                Price = Price,
                Exchange = Exchange
            };

            return companyShortInfo;
        }
    }
}
