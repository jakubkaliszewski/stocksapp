﻿using Stocks.Shared.Model.Models.Interfaces;
using SQLite;

namespace Stocks.Shared.Model.Models.Companies
{
    [Table("Companies")]
    public class Company : StockEntityObject
    {
        public string Symbol { get; set; }
        public decimal Price { get; set; }
        public decimal Beta { get; set; }
        public decimal VolumeAverage { get; set; }
        public decimal MarketCapitalization { get; set; }
        public decimal LastDividend { get; set; }
        public string WeekRange { get; set; }
        public decimal Changes { get; set; }
        public string ChangesInPercentage { get; set; }
        public string CompanyName { get; set; }
        public string Exchange { get; set; }
        public string Industry { get; set; }
        public string Website { get; set; }
        public string Description { get; set; }
        public string CEO { get; set; }
        public string Sector { get; set; }
        public string ImageUrl { get; set; }
    }
}
