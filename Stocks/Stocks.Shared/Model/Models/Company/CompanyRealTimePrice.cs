﻿using Stocks.Shared.Model.Models.Interfaces;

namespace Stocks.Shared.Model.Models.Companies
{
    public class CompanyRealTimePrice : StockObject
    {
        public string Symbol { get; set; }
        public decimal Price { get; set; }
    }
}
