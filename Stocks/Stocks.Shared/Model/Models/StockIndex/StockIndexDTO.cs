﻿using Stocks.Shared.Model.Models.Interfaces;

namespace Stocks.Shared.Model.Models.StockIndex
{
    public class StockIndexDTO : StockObjectDTO<StockIndex>
    {
        public string Ticker { get; set; }
        public decimal Changes { get; set; }
        public decimal Price { get; set; }
        public string IndexName { get; set; }

        public override StockIndex ToStockObject()
        {
            StockIndex stockIndex = new StockIndex()
            {
                ShortName = Ticker,
                Changes = Changes,
                Price = Price,
                IndexName = IndexName
            };
            return stockIndex;
        }
    }
}
