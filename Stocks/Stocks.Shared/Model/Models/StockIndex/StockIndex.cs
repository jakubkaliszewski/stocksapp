﻿using SQLite;
using Stocks.Shared.Model.Models.Interfaces;

namespace Stocks.Shared.Model.Models.StockIndex
{
    [Table("StockIndexes")]
    public class StockIndex : StockEntityObject
    {
        public string ShortName { get; set; }
        public decimal Changes { get; set; }
        public decimal Price { get; set; }
        public string IndexName { get; set; }

        public override string ToString()
        {
            string description = $"{ShortName}\nChanges: {Changes}%\nActual price: {Price}$\nIndex name: {IndexName}";
            return description;
        }
    }
}
