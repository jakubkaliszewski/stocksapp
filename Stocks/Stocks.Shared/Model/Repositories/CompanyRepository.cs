﻿using Stocks.Shared.Model.DatabaseContext;
using Stocks.Shared.Model.Models.Companies;

namespace Stocks.Shared.Model.Repositories
{
    class CompanyRepository : AbstractRepository<Company, int>
    {
        public CompanyRepository(SQLiteDbConnection SQLiteDbConnection) : base(SQLiteDbConnection)
        {
        }        
    }
}
