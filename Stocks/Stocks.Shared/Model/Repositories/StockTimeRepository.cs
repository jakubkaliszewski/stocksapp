﻿using Stocks.Shared.Model.DatabaseContext;
using Stocks.Shared.Model.Models.StockTime;

namespace Stocks.Shared.Model.Repositories
{
    class StockTimeRepository : AbstractRepository<StockTime, int>
    {
        public StockTimeRepository(SQLiteDbConnection SQLiteDbConnection) : base(SQLiteDbConnection)
        {
        }
    }
}
