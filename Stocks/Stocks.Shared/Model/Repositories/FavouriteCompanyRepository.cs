﻿using Stocks.Shared.Model.DatabaseContext;
using Stocks.Shared.Model.Models.Companies;

namespace Stocks.Shared.Model.Repositories
{
    class FavouriteCompanyRepository : AbstractRepository<FavouriteCompany, int>
    {
        public FavouriteCompanyRepository(SQLiteDbConnection SQLiteDbConnection) : base(SQLiteDbConnection)
        {
        }        
    }
}
