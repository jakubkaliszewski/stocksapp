﻿using Stocks.Shared.Model.DatabaseContext;
using Stocks.Shared.Model.Models.Interfaces;
using Stocks.Shared.Model.Queries;
using System;
using System.Collections.Generic;

namespace Stocks.Shared.Model.Repositories
{
    /// <summary>
    /// Podstawowa klasa abstrakcyjna dla każdego z repozytoriów.
    /// </summary>
    /// <typeparam name="Type"> Typ obiektu, na którym operuje repozytorium.</typeparam>
    /// <typeparam name="IdType"> Typ indeksu obiektu, na którym operuje repozytorium.</typeparam>
    abstract class AbstractRepository<T, IdType>
        where T :  StockEntityObject, new()
    {
        protected SQLiteDbConnection _SQLiteDbConnection;
        public AbstractRepository(SQLiteDbConnection SQLiteDbConnection)
        {
            _SQLiteDbConnection = SQLiteDbConnection;
        }

        public virtual void Add(T item)
        {
            _SQLiteDbConnection.Db.Insert(item);
            Console.WriteLine("{0}", item);
        }

        public virtual void Add(IEnumerable<T> items)
        {
            _SQLiteDbConnection.Db.InsertAll(items);
            Console.WriteLine("{0}", items);
        }

        public virtual void Delete(T item)
        {
            _SQLiteDbConnection.Db.Delete(item);
        }

        public virtual void DeleteAll(params T[] items)
        {
            if (items.Length > 0)
            {
                foreach (var item in items)
                {
                    _SQLiteDbConnection.Db.Delete(item);
                }
            }
            else
                _SQLiteDbConnection.Db.DeleteAll<T>();
        }

        public virtual void Update(T item)
        {
            _SQLiteDbConnection.Db.Update(item);
        }

        #region Get
        public virtual T GetById(IdType id)
        {
            T item = _SQLiteDbConnection.Db.Get<T>(id);
            return item;
        }

        public virtual IEnumerable<T> GetByQuery(IQuery<T> query)
        {
            var table = _SQLiteDbConnection.Db.Table<T>();
            string builtSelectQuery = query.BuildQuery(table.Table.TableName);
            var items = _SQLiteDbConnection.Db.Query<T>(builtSelectQuery);
            return items;
        }

        public virtual IEnumerable<T> GetAll()
        {
            var table = _SQLiteDbConnection.Db.Table<T>();
            var items = _SQLiteDbConnection.Db.Query<T>($"SELECT * FROM {table.Table.TableName}");
            return items;
        }
        #endregion
    }
}
