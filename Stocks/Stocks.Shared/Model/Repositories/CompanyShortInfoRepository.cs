﻿using Stocks.Shared.Model.DatabaseContext;
using Stocks.Shared.Model.Models.Companies;

namespace Stocks.Shared.Model.Repositories
{
    class CompanyShortInfoRepository : AbstractRepository<CompanyShortInfo, int>
    {
        public CompanyShortInfoRepository(SQLiteDbConnection SQLiteDbConnection) : base(SQLiteDbConnection)
        {
        }        
    }
}
