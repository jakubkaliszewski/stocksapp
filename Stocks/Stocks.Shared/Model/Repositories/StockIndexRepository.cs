﻿using Stocks.Shared.Model.DatabaseContext;
using Stocks.Shared.Model.Models.StockIndex;

namespace Stocks.Shared.Model.Repositories
{
    class StockIndexRepository : AbstractRepository<StockIndex, int>
    {
        public StockIndexRepository(SQLiteDbConnection SQLiteDbConnection) : base(SQLiteDbConnection)
        {
        }
    }
}
