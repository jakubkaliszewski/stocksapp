﻿using Stocks.Shared.Model.DatabaseContext;
using Stocks.Shared.Model.Models.StockCompanies;

namespace Stocks.Shared.Model.Repositories
{
    class StockCompanyRepository : AbstractRepository<StockCompany, int>
    {
        public StockCompanyRepository(SQLiteDbConnection SQLiteDbConnection) : base(SQLiteDbConnection)
        {
        }       
    }
}
