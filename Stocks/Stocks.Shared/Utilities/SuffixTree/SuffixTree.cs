﻿using System.Collections.Generic;
using System.Linq;

namespace Stocks.Shared.Utilities.SuffixTree
{
    class SuffixTree
    {
        private SuffixTreeNode _root;
        private SuffixTreeNode _lastNewNode;

        private SuffixTreeNode _activeNode;
        private int _activeEdge = -1;
        private int _activeLength = 0;

        private int _remainingSuffixCount = 0;
        private ReferenceInt _end = new ReferenceInt(-1);
        private int _currentId = 0;

        public string Text { get; private set; }
        
        public SuffixTree()
        {
        }

        private void SetDefaults()
        {
            _root = null;
            _lastNewNode = null;
            _activeNode = null;
            _activeEdge = -1;
            _activeLength = 0;
            _remainingSuffixCount = 0;
            _end = new ReferenceInt(-1);
            Text = "";
            _currentId = 0;
        }

        public void Build(string text)
        {
            if (_root != null)
                SetDefaults();

            Text = text;
            _root = CreateNewNode(-1, -1);
            _activeNode = _root;

            for (int i = 0; i < Text.Length; i++)
            {
                ExtendSuffixTree(i);
            }
            SetIndexByDFS(_root, 0);
        }

        private void SetIndexByDFS(SuffixTreeNode node, int depth)
        {
            if (node == null)
                return;

            if (!node.Children.Any())
                node.Index = Text.Length - depth;
            else
            {
                foreach (var child in node.Children.Values)
                {
                    SetIndexByDFS(child, depth + child.EdgeLength);
                }
            }
        }


        private SuffixTreeNode CreateNewNode(int start, ReferenceInt end)
        {
            SuffixTreeNode node = new SuffixTreeNode()
            {
                Id = _currentId++,
                Link = _root,
                Start = start,
                End = end,
                Index = -1
            };

            return node;
        }

        private bool WalkDown(SuffixTreeNode node)
        {
            if (_activeLength >= node.EdgeLength)
            {
                _activeEdge += node.EdgeLength;
                _activeLength -= node.EdgeLength;
                _activeNode = node;
                return true;
            }
            return false;
        }

        private void ExtendSuffixTree(int letterIndex)
        {
            _end.Value = letterIndex;
            _remainingSuffixCount++;
            _lastNewNode = null;

            while (_remainingSuffixCount > 0)
            {
                if (_activeLength == 0)
                    _activeEdge = letterIndex;

                if (!_activeNode.Children.ContainsKey(Text[_activeEdge]))
                {
                    var newNode = CreateNewNode(letterIndex, _end);
                    newNode.Parent = _activeNode;
                    _activeNode.Children.Add(Text[_activeEdge], newNode);
                    if (_lastNewNode != null)
                    {
                        _lastNewNode.Link = _activeNode;
                        _lastNewNode = null;
                    }
                }
                else
                {
                    var next = _activeNode.Children[Text[_activeEdge]];
                    if (WalkDown(next))
                        continue;

                    if (Text[next.Start + _activeLength] == Text[letterIndex])
                    {
                        if (_lastNewNode != null && _activeNode != _root)
                        {
                            _lastNewNode.Link = _activeNode;
                            _lastNewNode = null;
                        }

                        _activeLength++;
                        break;
                    }

                    var splitEnd = next.Start + _activeLength - 1;
                    var split = CreateNewNode(next.Start, splitEnd);
                    _activeNode.Children[Text[_activeEdge]] = split;
                    split.Parent = _activeNode;

                    var newNode = CreateNewNode(letterIndex, _end);
                    newNode.Parent = split;
                    split.Children[Text[letterIndex]] = newNode;

                    next.Start += _activeLength;
                    split.Children[Text[next.Start]] = next;
                    next.Parent = split;

                    if (_lastNewNode != null)
                        _lastNewNode.Link = split;

                    _lastNewNode = split;
                }

                _remainingSuffixCount--;
                if (_activeNode == _root && _activeLength > 0)
                {
                    _activeLength--;
                    _activeEdge = letterIndex - _remainingSuffixCount + 1;
                }
                else if (_activeNode != _root)
                    _activeNode = _activeNode.Link;
            }
        }

        public List<int> FindSubstring(string substring)
        {
            List<int> indices = new List<int>();
            if (_root != null)
            {
                FindSubstring(_root, ref substring, 0, indices);
                indices.Sort();
            }

            return indices;
        }

        private void FindSubstring(SuffixTreeNode node, ref string substring, int charIndex, List<int> indices)
        {
            if (charIndex >= substring.Length)
                return;

            if (node.Children.TryGetValue(substring[charIndex], out SuffixTreeNode child))
            {
                int index = TraverseEdge(child.Start, child.End, charIndex, ref substring);
                if (index >= 0)
                {
                    GoToLeaves(child, indices);
                    return;
                }
                else if (index == -1)
                    charIndex += child.EdgeLength;
                else if (index == -2)
                    return;

                FindSubstring(child, ref substring, charIndex, indices);
            }
        }

        private void GoToLeaves(SuffixTreeNode node, List<int> indices)
        {
            if (node != null)
            {
                if (node.Index > -1)
                {
                    indices.Add(node.Index);
                    return;
                }

                foreach (var child in node.Children.Values)
                {
                    GoToLeaves(child, indices);
                }
            }
        }

        private int TraverseEdge(int start, int end, int charIndex, ref string substring)
        {
            int j = charIndex;
            int i = start;
            for (; i <= end && j < substring.Length; i++, j++)
            {
                if (substring[j] != Text[i])
                    return -2;
            }

            if (j == substring.Length)
                return i - substring.Length;

            return -1;
        }

        public (List<int> startIndices, int length) GetLongestRepeatedSubstring()
        {
            int maxDepth = 0;
            var startIndices = new List<int>();
            GetLongestRepeatedSubstring(_root, 0, ref maxDepth, startIndices);
            startIndices.Sort();
            return (startIndices, maxDepth);
        }

        private void GetLongestRepeatedSubstring(SuffixTreeNode node, int depth, ref int maxDepth, List<int> startIndices)
        {
            if (node != null)
            {
                if (node.Index == -1)
                {
                    foreach (var child in node.Children.Values)
                    {
                        GetLongestRepeatedSubstring(child, depth + child.EdgeLength, ref maxDepth, startIndices);
                    }
                }
                else if (node.Index > -1 && (maxDepth < depth - node.EdgeLength))
                {
                    maxDepth = depth - node.EdgeLength;
                    startIndices.Clear();
                    GoToLeaves(node.Parent, startIndices);
                }
            }
        }
    }
}
