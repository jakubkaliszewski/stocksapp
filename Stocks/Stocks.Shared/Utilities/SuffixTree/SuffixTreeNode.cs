﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Stocks.Shared.Utilities.SuffixTree
{
    class ReferenceInt
    {
        public int Value { get; set; }

        public ReferenceInt(int value)
        {
            Value = value;
        }

        public static implicit operator int(ReferenceInt referenceInt)
        {
            return referenceInt.Value;
        }

        public static implicit operator ReferenceInt(int value)
        {
            return new ReferenceInt(value);
        }
    }

    class SuffixTreeNode
    {
        public int Id { get; set; }
        public Dictionary<char, SuffixTreeNode> Children { get; set; }
        public SuffixTreeNode Link { get; set; }
        public int Start { get; set; }
        public ReferenceInt End { get; set; }
        public int Index { get; set; }
        public int EdgeLength => End - Start + 1;

        public SuffixTreeNode Parent { get; internal set; }

        public SuffixTreeNode()
        {
            Children = new Dictionary<char, SuffixTreeNode>();
        }        
    }
}
