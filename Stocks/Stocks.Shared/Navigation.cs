﻿using System;
using System.Collections.Generic;
using System.Text;
using Windows.UI.Xaml.Controls;

namespace Stocks.Shared
{
    public static class Navigation
    {
        private static Frame _frame;
       // private static readonly EventHandler<BackRequestedEventArgs> GoBackHandler = (s, e) => Navigation.GoBack();
        //private static readonly EventHandler<BackPressedEventArgs> GoBackPhoneHandler = (s, e) => Navigation.GoBack(e);

        public static Frame Frame
        {
            get { return _frame; }
            set { _frame = value; }
        }

        public static bool Navigate(Type sourcePageType)
        {
            if (_frame.CurrentSourcePageType != sourcePageType)
            {
                return _frame.Navigate(sourcePageType);
            }

            return true;
        }

       
    }
}
