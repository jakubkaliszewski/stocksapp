﻿using Stocks.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Stocks.Shared.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class TopNewsPage : Page
    {
        public TopNewsPage()
        {
            this.InitializeComponent();
        }

        private void GridView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var model = (NewsPageViewModel)DataContext;
            model.OpenNewsWebsite(e.ClickedItem);
        }

        private void GridView_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            var columns = Math.Ceiling(ActualWidth / ((ItemsWrapGrid)NewsGridView.ItemsPanelRoot).ItemWidth);
            ((ItemsWrapGrid)NewsGridView.ItemsPanelRoot).ItemWidth = e.NewSize.Width / columns;
        }

        private void LoadMoreButton_Click(object sender, RoutedEventArgs e)
        {
            var model = (NewsPageViewModel)DataContext;
            model.LoadMoreNews();
        }
    }
}
