﻿using Stocks.Shared.ViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace Stocks.Shared.Pages
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class CompanyPage : Page
    {
        public CompanyPage()
        {
            this.InitializeComponent();            
        }

        private async void LikeButton_Click(object sender, RoutedEventArgs e)
        {
            var model = (DataContext as CompanyPageViewModel);
            await model.AddToFavourites();
            LikeButton.Visibility = Visibility.Collapsed;
            UnlikeButton.Visibility = Visibility.Visible;
            Stocks.MainPage.RefreshData();
        }

        private void UnlikeButton_Click(object sender, RoutedEventArgs e)
        {
            var model = (DataContext as CompanyPageViewModel);
            model.RemoveFromFavourites();
            LikeButton.Visibility = Visibility.Visible;
            UnlikeButton.Visibility = Visibility.Collapsed;
            Stocks.MainPage.RefreshData();
        }

        private async void btnSearch_Click(object sender, RoutedEventArgs e)
        {            
            await (DataContext as CompanyPageViewModel).GetHistoricalData();
        }

        private async void btnShowLastWeek_Click(object sender, RoutedEventArgs e)
        {
            await (DataContext as CompanyPageViewModel).GetHistoricalData(StocksTimeSpan.Week);
        }

        private async void btnShowLastMonth_Click(object sender, RoutedEventArgs e)
        {
            await (DataContext as CompanyPageViewModel).GetHistoricalData(StocksTimeSpan.Month);
        }

        private async void btnShowLastYear_Click(object sender, RoutedEventArgs e)
        {
            await (DataContext as CompanyPageViewModel).GetHistoricalData(StocksTimeSpan.Year);
        }

        private async void btnShowLastRecords_Click(object sender, RoutedEventArgs e)
        {
            await (DataContext as CompanyPageViewModel).GetHistoricalData(StocksTimeSpan.SpecifiedByLastRecords);
        }

        private void CompanyPage_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            MainGrid.Height = e.NewSize.Height;
            //MainScroller.Height = e.NewSize.Height * 0.9;
            //PanelStocks.Height = e.NewSize.Height * 0.9;            
        }

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            string companyName = e.Parameter as string;
            var model = (DataContext as CompanyPageViewModel);
            model.CompanySymbol = companyName;
            if (model.IsFavourite())
            {
                LikeButton.Visibility = Visibility.Collapsed;
                UnlikeButton.Visibility = Visibility.Visible;
            }
            else
            {
                LikeButton.Visibility = Visibility.Visible;
                UnlikeButton.Visibility = Visibility.Collapsed;
            }
        }


    }
}
