﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json.Linq;
using Stocks.Shared.Model.Models.StockTime;
using Stocks.Shared.Model.Repositories;
using Stocks.Shared.Model.Queries;
using Stocks.Shared.Model.Models.Companies;
using Stocks.Shared.Model.Models.StockIndex;
using Stocks.Shared.Model.Models.StockCompanies;
using Stocks.Shared.Model.Models.Interfaces;
using Stocks.Shared.WebAPI.StockTimeAPI;
using Uno.Extensions;

namespace Stocks.Shared.WebAPI
{
    public class DataGetter
    {
        private static readonly string COMPANY_URL = "https://financialmodelingprep.com/api/v3/company/profile/";
        private static readonly string STOCK_INDEX_URL = "https://financialmodelingprep.com/api/v3/majors-indexes/";
        private static readonly string MOST_ACTIVE_STOCKS_URL = "https://financialmodelingprep.com/api/v3/stock/actives";
        private static readonly string MOST_GAINER_STOCKS_URL = "https://financialmodelingprep.com/api/v3/stock/gainers";
        private static readonly string MOST_LOSER_STOCKS_URL = "https://financialmodelingprep.com/api/v3/stock/losers";
        private static readonly string STOCK_HISTORICAL_PRICE_URL = "https://financialmodelingprep.com/api/v3/historical-price-full/";
        private static readonly string STOCK_ALL_COMPANIES_URL = "https://financialmodelingprep.com/api/v3/company/stock/list";
        private static readonly string STOCK_REAL_TIME_PRICES_URL = "https://financialmodelingprep.com/api/v3/stock/real-time-price/";
        private static readonly string API_KEY = "f524061b097fac7bf2e960a1276dabdf";
        private static readonly string API_KEY_2 = "0b7213f86c8be1d9b3f8cd1dc430faf7";
        public async Task<IEnumerable<StockTime>> GetStockTimes(string symbol, StockTimeAPIQuery query)
        {
            string args = query?.ToString();
            args = (String.IsNullOrWhiteSpace(args)) ? "" : "?" + args;
            string url = STOCK_HISTORICAL_PRICE_URL + symbol + args;
            string property = "historical";
            var stockTimes = await GetObjectList<StockTime, StockTimeDTO>(url, property);

            var repository = DependencyInjection.Container.GetInstance<StockTimeRepository>();

            var dbQuery = new StockTimeQuery()
            {
                LastRecords = 3,
                StartDate = new DateTime(2020, 3, 19),
                EndDate = new DateTime(2020, 3, 26)
            };
            var stockTimesDB = repository.GetByQuery(dbQuery);

            return stockTimes;
        }              

        public async Task<CompanyRealTimePrice> GetCompanyRealTimePrice(string symbol)
        {
            string url = STOCK_REAL_TIME_PRICES_URL + symbol;
            var company = await GetObject<CompanyRealTimePrice, CompanyRealTimePriceDTO>(url);
            return company;
        }

        public async Task<IEnumerable<CompanyRealTimePrice>> GetAllCompaniesRealTimePrices()
        {
            string url = STOCK_REAL_TIME_PRICES_URL;
            string property = "stockList";
            var companies = await GetObjectList<CompanyRealTimePrice, CompanyRealTimePriceDTO>(url, property);
            return companies;
        }

        public async Task<IEnumerable<CompanyShortInfo>> GetAllCompanies()
        {
            string url = STOCK_ALL_COMPANIES_URL;
            string property = "symbolsList";
            var repository = DependencyInjection.Container.GetInstance<CompanyShortInfoRepository>();
            var companies = repository.GetAll();
            if (!companies.Any())
            {
                companies = await GetObjectList<CompanyShortInfo, CompanyShortInfoDTO>(url, property);
                repository.Add(companies);
            }
            return companies;
        }

        public async Task<Company> GetCompany(string symbol)
        {
            var repository = DependencyInjection.Container.GetInstance<CompanyRepository>();
            var query = new CompanyQuery() { Symbol = symbol };
            var company = repository.GetByQuery(query).SingleOrDefault();
            if (company == null)
            {
                string url = COMPANY_URL + symbol;
                company = await GetObject<Company, CompanyDTO>(url);
                repository.Add(company);
            }
            return company;
        }

        public async Task<StockIndex> GetStockIndex(string symbol)
        {
            string url = STOCK_INDEX_URL + symbol;
            StockIndex stockIndex = await GetObject<StockIndex, StockIndexDTO>(url);
            return stockIndex;
        }

        public async Task<IEnumerable<StockIndex>> GetStockIndexes()
        {
            string url = STOCK_INDEX_URL;
            string property = "majorIndexesList";
            var stockIndexes = await GetObjectList<StockIndex, StockIndexDTO>(url, property);
            return stockIndexes;
        }

        public async Task<IEnumerable<StockCompany>> GetMostActiveStockCompanies()
        {
            string url = MOST_ACTIVE_STOCKS_URL;
            string property = "mostActiveStock";
            var stockCompanies = await GetObjectList<StockCompany, StockCompanyDTO>(url, property);
            return stockCompanies;
        }

        public async Task<IEnumerable<StockCompany>> GetMostGainerStockCompanies()
        {
            string url = MOST_GAINER_STOCKS_URL;
            string property = "mostGainerStock";
            var stockCompanies = await GetObjectList<StockCompany, StockCompanyDTO>(url, property);
            return stockCompanies;
        }

        public async Task<IEnumerable<StockCompany>> GetMostLoserStockCompanies()
        {
            string url = MOST_LOSER_STOCKS_URL;
            string property = "mostLoserStock";
            var stockCompanies = await GetObjectList<StockCompany, StockCompanyDTO>(url, property);
            return stockCompanies;
        }

        private async Task<Type> GetObject<Type, TypeDTO>(string url)
            where Type : StockObject
            where TypeDTO : StockObjectDTO<Type>
        {
            string data = await GetData(url);
            TypeDTO stockObjectDTO = JsonConvert.DeserializeObject<TypeDTO>(data);
            Type stockObject = stockObjectDTO.ToStockObject();
            return stockObject;
        }

        private async Task<IEnumerable<Type>> GetObjectList<Type, TypeDTO>(string url, string property = "")
            where Type : StockObject
            where TypeDTO : StockObjectDTO<Type>
        {
            string data = await GetData(url);
            JObject jObject;
            if (!property.IsNullOrWhiteSpace())
            {
                jObject = JObject.Parse(data);
                if (jObject != null && jObject[property] != null)
                    data = jObject[property].ToString();
            }

            IEnumerable<Type> stockObjects = new List<Type>();

            if (data != "{{}}")
            {
                var stockObjectDTOs = JsonConvert.DeserializeObject<IEnumerable<TypeDTO>>(data);
                stockObjects = stockObjectDTOs.Select(x => x.ToStockObject());
            }
            return stockObjects;
        }

        private async Task<string> GetData(string url)
        {
            if (!url.Contains("?"))
                url += "?";
            else
                url += "&";
            url += "apikey=" + API_KEY;

            string result = "";
            try
            {
                result = await GetData(url, result);
            }
            catch (Exception)
            {
                url = url.Replace(API_KEY, API_KEY_2);
                result = await GetData(url, result);
            }

            if (String.IsNullOrWhiteSpace(result))
            {
                url = url.Replace(API_KEY, API_KEY_2);
                result = await GetData(url, result);
            }

            return result;
        }

        private static async Task<string> GetData(string url, string result)
        {
            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), url))
                {
                    request.Headers.TryAddWithoutValidation("Access-Control-Request-Headers", "1");

                    var response = await httpClient.SendAsync(request);
                    result = await response.Content.ReadAsStringAsync();
                }
            }

            return result;
        }
    }
}
