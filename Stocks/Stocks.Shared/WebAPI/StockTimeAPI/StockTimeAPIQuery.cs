﻿using System;
using System.Collections.Generic;

namespace Stocks.Shared.WebAPI.StockTimeAPI
{
    public class StockTimeAPIQuery
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int LastRecords { get; set; }
        public string SerieType { get; set; }
        public string SerieFormat { get; set; }

        public override string ToString()
        {
            var args = new List<string>();

            if (StartDate != default)
                args.Add($"from={StartDate:yyyy-MM-dd}");

            if (EndDate != default)
                args.Add($"to={EndDate:yyyy-MM-dd}");

            if (LastRecords > 0)
                args.Add($"timeseries={LastRecords}");

            if (!String.IsNullOrWhiteSpace(SerieType))
                args.Add($"serietype={SerieType}");

            if (!String.IsNullOrWhiteSpace(SerieFormat))
                args.Add($"serieformat={SerieFormat}");

            string result = String.Join("&", args);
            return result;
        }
    }
}
