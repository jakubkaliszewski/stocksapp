﻿using Stocks.Shared.Model.Models.News;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Net.Http;
using System.Threading.Tasks;

namespace Stocks.Shared.WebAPI
{
    public class NewsGetter
    {
        private static readonly string NEWS_URL = "http://newsapi.org/v2/top-headlines?category=business&country=us";
        private static readonly string API_KEY = "d4ce7df145af47e99bdc22d289e6f955";

        public int TotalResults { get; set; }

        public async Task<IEnumerable<NewsEntry>> GetNews()
        {
            string url = NEWS_URL + "&apiKey=" + API_KEY;

            NewsObject newsObject = await GetData(url);
            TotalResults = newsObject.TotalResults;

            return newsObject.Articles;
        }

        public async Task<IEnumerable<NewsEntry>> GetMoreNews(int page)
        {
            string url = NEWS_URL + "&page=" + page + "&apiKey=" + API_KEY;

            return (await GetData(url)).Articles;
        }

        public async Task<NewsObject> GetData(string url)
        {
            NewsObject newsObject;

            using (var httpClient = new HttpClient())
            {
                using (var request = new HttpRequestMessage(new HttpMethod("GET"), url))
                {
                    request.Headers.TryAddWithoutValidation("Access-Control-Request-Headers", "1");

                    var response = await httpClient.SendAsync(request);
                    string result = await response.Content.ReadAsStringAsync();
                    newsObject = JsonConvert.DeserializeObject<NewsObject>(result);
                }
            }

            return newsObject;
        }
    }
}
