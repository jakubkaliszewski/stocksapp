﻿using Microsoft.ML;
using Microsoft.ML.Data;
using Microsoft.ML.Transforms.TimeSeries;
using Stocks.Shared.Model.Models.StockTime;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Stocks.Shared
{
    public class Data
    {
        [LoadColumn(0)]
        public DateTime Date { get; set; }

        [LoadColumn(1)]
        public float Value { get; set; }
    }

    public class Forecast
    {
        public float[] Values { get; set; }
    }

    public static class Prediction
    {
        public static decimal[] Predict(List<StockTime> stocks)
        {
            var context = new MLContext();

            var entries = stocks.Select(stock => new Data() { Date = stock.Date, Value = (float)stock.Close }).ToList();
            var data = context.Data.LoadFromEnumerable(entries);


            var windowSize = stocks.Count / 4;
            var seriesLength = windowSize * 2;
            var trainSize = stocks.Count;
            var horizon = windowSize;

            var pipeline = context.Forecasting.ForecastBySsa(nameof(Forecast.Values), nameof(Data.Value), windowSize, seriesLength, trainSize, horizon);
            var model = pipeline.Fit(data);

            var forecastEngine = model.CreateTimeSeriesEngine<Data, Forecast>(context);
            var forecasts = forecastEngine.Predict();

            if (forecasts.Values.All(val => val == 0))
                throw new ArgumentException("Could not predict stocks.");

            return forecasts.Values.Select(x => decimal.Round((decimal)x, 2)).ToArray();
        }
    }
}
