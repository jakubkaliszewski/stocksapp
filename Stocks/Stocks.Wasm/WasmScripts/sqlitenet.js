﻿//var sqljs = require('./sql.js');
require('./sql.js');

var databaseName = null;
var SQLiteNative = null;//nie używamy
var db = null;
var loaded = false;

window.onbeforeunload = saveToLocalStorage();

/*
	<script src="./sql.js"></script>
<script>
	var connection = initSqlJs().then((SQL) => {
		let db = new SQL.Database();

		let sqlstr = "CREATE TABLE hello (a int, b char);";
		sqlstr += "INSERT INTO hello VALUES (0, 'hello');"
		sqlstr += "INSERT INTO hello VALUES (1, 'world');"
		db.run(sqlstr); // Run the query without returning anything

		let array = db.export();
		console.log(array);
		//localStorage.setItem('db', array.toString());
	});
</script>
*/

function loadFromLocalStorage(){
    let dataString = localStorage.getItem(databaseName);
    dataString = dataString.split(',');
    return new Uint8Array(dataString);
}

function saveToLocalStorage(){
    let data = db.export();
    localStorage.setItem(databaseName, data.toString());
}

class SQLiteNet {
    static sqliteLibVersionNumber() {
        SQLiteNet.ensureInitialized();
        return SQLiteNative.Database.libversion_number();
    }

    static ensureInitialized() {
        if (!SQLiteNative) {
            SQLiteNative = require('sql');
        }
    }

    //Wczytujemy bazę danych z pamięci podręcznej, jeśli pusta to utworzy się nowa baza. Inicjalizacja
    static sqliteOpen(fileName) {
        if (db === null && databaseName === null && SQLiteNative === null) {
            //dokonujemy wczytania
            initSqlJs().then((SQL) => {
                databaseName = fileName;
                db = new SQL.Database(loadFromLocalStorage);
                loaded = true;
            });
        }

        /*
        if (FS.findObject(fileName) !== null) {

            // Read the whole file in the mono module in memory
            const binaryDb = FS.readFile(fileName, { encoding: 'binary', flags: "" });

            var r1 = SQLiteNative.Database.open(fileName, binaryDb, { mode: "rwc", cache: "private" });

            fileMap[r1.pDB] = fileName;

            return `${r1.Result};${r1.pDB}`;
        }
        else {
            var r2 = SQLiteNative.Database.open(fileName, null, { mode: "rwc", cache: "private" });

            fileMap[r2.pDB] = fileName;

            return `${r2.Result};${r2.pDB}`;
        }
         */
    }

    static sqliteClose2(pDb) {
        try {
            saveToLocalStorage();
            db.close();
            db = null;
            databaseName = null;
            loaded = false;
        } catch (e) {
            return false;
        }

        return true;
    }

    static sqlitePrepare2(pDb, query) {

        var stmt = SQLiteNative.Database.prepare2(pDb, query);

        return `${stmt.Result};${stmt.pStatement}`;
    }

    static sqliteChanges(dbId) {
        return SQLiteNative.Database.changes(dbId);
    }

    static sqliteErrMsg(dbId) {
        return SQLiteNative.Database.errmsg(dbId);
    }

    static sqliteLastInsertRowid(dbId) {
        return SQLiteNative.Database.last_insert_rowid(dbId);
    }

    static sqliteStep(pStatement) {
        return SQLiteNative.Database.step(pStatement);
    }

    static sqliteReset(pStatement) {
        return SQLiteNative.Database.reset(pStatement);
    }

    static sqliteFinalize(pStatement) {
        return SQLiteNative.Database.finalize(pStatement);
    }

    static sqliteColumnType(pStatement, index) {
        return SQLiteNative.Database.column_type(pStatement, index);
    }

    static sqliteColumnString(pStatement, index) {
        return SQLiteNative.Database.column_text(pStatement, index);
    }

    static sqliteColumnInt(pStatement, index) {
        return SQLiteNative.Database.column_int(pStatement, index);
    }

    static sqliteColumnCount(pStatement) {
        return SQLiteNative.Database.column_count(pStatement);
    }

    static sqliteColumnName(pStatement, index) {
        return SQLiteNative.Database.column_name(pStatement, index);
    }

    static sqliteBindText(pStatement, index, val) {
        return SQLiteNative.Database.bind_text(pStatement, index, val);
    }

    static sqliteBindNull(pStatement, index) {
        return SQLiteNative.Database.bind_null(pStatement, index);
    }

    static sqliteBindInt(pStatement, index, value) {
        return SQLiteNative.Database.bind_int(pStatement, index, value);
    }

    static sqliteBindInt64(pStatement, index, value) {
        return SQLiteNative.Database.bind_int64(pStatement, index, value);
    }

    static sqliteBindDouble(pStatement, index, value) {
        return SQLiteNative.Database.bind_double(pStatement, index, value);
    }

}
